
#include "flash.h"

#include "board.h"

#include <cr_section_macros.h>


uint8_t line[100]; // buffer for the input line
unsigned int line_ptr;
int char_buf;

static const char * const hexstr = "0123456789ABCDEF";

__attribute__ ((section(".after_vectors")))
void print_hex(uint8_t n) {
	Board_UARTPutChar(hexstr[n / 16]);
	Board_UARTPutChar(hexstr[n % 16]);
}

static uint8_t hex2byte(const uint8_t * const str)
{
	uint8_t val;
	if (str[0] >= 'a' && str[0] <='f') val = str[0] - 'a' + 10;
	else if (str[0] >= 'A' && str[0] <='F') val = str[0] - 'A' + 10;
	else val = str[0] - '0';
	val = val << 4;
	if (str[1] >= 'a' && str[1] <='f') val |= str[1] - 'a' + 10;
	else if (str[1] >= 'A' && str[1] <='F') val |= str[1] - 'A' + 10;
	else val |= str[1] - '0';
	return val;
}

static uint16_t hex2word(const uint8_t * const str)
{
	uint16_t val;
	if (str[0] >= 'a' && str[0] <='f') val = str[0] - 'a' + 10;
	else if (str[0] >= 'A' && str[0] <='F') val = str[0] - 'A' + 10;
	else val = str[0] - '0';
	val = val << 4;
	if (str[1] >= 'a' && str[1] <='f') val |= str[1] - 'a' + 10;
	else if (str[1] >= 'A' && str[1] <='F') val |= str[1] - 'A' + 10;
	else val |= str[1] - '0';
	val = val << 4;
	if (str[2] >= 'a' && str[2] <='f') val |= str[2] - 'a' + 10;
	else if (str[2] >= 'A' && str[2] <='F') val |= str[2] - 'A' + 10;
	else val |= str[2] - '0';
	val = val << 4;
	if (str[3] >= 'a' && str[3] <='f') val |= str[3] - 'a' + 10;
	else if (str[3] >= 'A' && str[3] <='F') val |= str[3] - 'A' + 10;
	else val |= str[3] - '0';
	return val;
}

void mode_erase(void) {
	Board_UARTPutSTR("erase not implemented\r\n\n");
}

void mode_dump(void) {
	uint32_t dump_address;
	uint16_t *dump_data;

	for (;;) {
		Board_UARTPutChar('D'); // prompt

		// read one line from serial, which should contain exactly 6 hex characters
		line_ptr = 0;
		for (;;) {
			while ((char_buf = Board_UARTGetChar()) == EOF); // loop until we actually have a char
			if (char_buf == '\r' || char_buf == '\n') break;
			line[line_ptr] = char_buf;
			++line_ptr;
			if (!(line_ptr < 99)) break;
		}
		line[line_ptr] = '\0';

		if (line_ptr != 6) {
			Board_UARTPutSTR("<err: bad address>");
			continue;
		}

		// assume that the 6 characters are correctly formatted
		dump_address = (hex2byte(line) << 16) | (hex2byte(line + 2) << 8) | hex2byte(line + 4);
		dump_data = SPI_Read_Sector(dump_address, 256);

		Board_UARTPutSTR("\r\n");
		for (uint16_t c = 0; c < 256; ++c) {
			if (!(c % 16)) {
				print_hex(dump_address >> 16);
				print_hex(dump_address >>  8);
				print_hex(dump_address >>  0);
				Board_UARTPutChar(' ');
			}
			if (!(c % 8)) {
				Board_UARTPutChar(' ');
			}
			print_hex(dump_data[c]);
			Board_UARTPutChar(' ');
			if (c % 16 == 15) {
				Board_UARTPutSTR("\r\n");
			}
			dump_address = (dump_address + 16) % 0x1000000;
		}
		Board_UARTPutSTR("\r\n");

		break;
	}
}

#define STATE_BEGIN 0
#define STATE_READ  1
#define STATE_FLUSH 2

void mode_program(void) {
	uint8_t running_checksum;
	uint8_t byte_buf[32];
	unsigned int byte_count;
	unsigned int addr_l;
	unsigned int record_type;
	uint32_t address;

	uint32_t data_base;
	uint16_t *data_ptr;
	uint8_t *data_src;
	unsigned int data_counter;

	unsigned int current_state = STATE_BEGIN;
	uint32_t addr_h = 0;

    for (;;) {
    	Board_UARTPutChar('.'); // prompt


    	// read one line from serial, in Intel HEX 32 format
    	line_ptr = 0;
    	for (;;) {
        	while ((char_buf = Board_UARTGetChar()) == EOF); // loop until we actually have a char
        	if (char_buf == '\r' || char_buf == '\n') break;
        	line[line_ptr] = char_buf;
        	++line_ptr;
        	if (!(line_ptr < 99)) break;
    	}
    	line[line_ptr] = '\0';

    	// parse intel hex format
    	if (line_ptr < 11) {
    		Board_UARTPutSTR("<err: format line too short>");
    		continue;
    	}
    	if (line_ptr > 75) {
    		Board_UARTPutSTR("<err: format line too long>");
    		continue;
    	}
    	if (line[0] != ':') {
    		Board_UARTPutSTR("<err: format missing start code>");
    		continue;
    	}

    	{
    		unsigned int temp;
			running_checksum  = (byte_count = hex2byte(line + 1));
			running_checksum += (addr_l = hex2byte(line + 3));
			temp = hex2byte(line + 5);
			addr_l = (addr_l << 8) | temp; running_checksum += temp;
			running_checksum += (record_type = hex2byte(line + 7));
    	}

    	// the length of a line should be 11 plus the byte count
    	if (line_ptr != (byte_count * 2) + 11) {
    		Board_UARTPutSTR("<err: format wrong length>");
    		continue;
    	}

    	for (unsigned int c = 0; c < byte_count; ++c) {
    		running_checksum += (byte_buf[c] = hex2byte(line + (c * 2) + 9));
    	}
    	running_checksum = (~running_checksum) + 1;

    	if (running_checksum != hex2byte(line + line_ptr - 2)) {
    		Board_UARTPutSTR("<err: checksum>");
    		continue;
    	}

    	switch (record_type) {
    	case 0:
    		Board_UARTPutChar('K');
    		break;
    	case 1:
    		Board_UARTPutChar('E');
    		if (current_state == STATE_READ) {
    			SPI_Write_Sector(data_base);
    		    Board_LED_Set(0, true);
    			Board_UARTPutSTR('X');
    		}
    		return;
    	case 4:
    		Board_UARTPutChar('C');
    		addr_h = (byte_buf[0] << 24) | (byte_buf[1] << 16);
    		continue;
    	default:
    		Board_UARTPutSTR("<err: unknown record type>");
    		continue;
    	}

    	address = addr_h | addr_l;

    	switch (current_state) {
    	/**
    	 * In the BEGIN state, we are reading the first set of bytes in a 256-byte page.
    	 * Take note of the starting address. We can only read 256 before a flush must be done.
    	 */
    	case STATE_BEGIN:
    		Board_UARTPutChar("0123456789ABCDEF"[(data_counter >> 4) % 16]);
    		data_base = address;
    		data_ptr = TxBuf + 4;
    		data_src = byte_buf;
    		for (data_counter = 0; data_counter < byte_count; ++data_counter) {
    			*data_ptr++ = *data_src++;
    		}
    		current_state = STATE_READ;
    		break;
		/**
		 * In the READ state, we are reading the next set of bytes in a 256-byte page.
		 * Take note of the starting address. We can only read 256 before a flush must be done.
		 */
    	case STATE_READ:
    		Board_UARTPutChar("0123456789ABCDEF"[(data_counter >> 4) % 16]);
    		data_src = byte_buf;
    		for (int c = 0; c < byte_count; ++c, ++data_counter) {
    			*data_ptr++ = *data_src++;
    		}
    		if (data_counter >= 256) {
    			current_state = STATE_FLUSH;
    		} else {
    			break;
    		}
    	case STATE_FLUSH:
    		Board_UARTPutChar('F');
    		Board_UARTPutChar("0123456789ABCDEF"[(data_base >> 8) % 16]);
    		Board_UARTPutSTR("\r\n");
    		SPI_Write_Sector(data_base);
    		data_counter = 0;
    		current_state = STATE_BEGIN; // TODO: handle partial page writes
    		break;
    	}
    	// get number of bytes
    }
}

int main(void) {
    SystemCoreClockUpdate();
    Board_Init();
    Board_LED_Set(0, false);
    Board_LED_Set(1, false);
    Board_LED_Set(2, false);

    Board_UARTPutSTR("\r\nCustom Flash Utility\r\n");

    Init_SPI();

    for (;;) {
		Board_UARTPutChar('?'); // prompt

		// read one line from serial
		line_ptr = 0;
		for (;;) {
			while ((char_buf = Board_UARTGetChar()) == EOF); // loop until we actually have a char
			if (char_buf == '\r' || char_buf == '\n') break;
			line[line_ptr] = char_buf;
			++line_ptr;
			if (!(line_ptr < 99)) break;
		}
		line[line_ptr] = '\0';

		switch (line[0]) {
		case 'e':
			mode_erase();
			continue;
		case 'd':
			mode_dump();
			continue;
		case 'p':
			mode_program();
			continue;
		default:
			continue;
		}
    }


    while (1);




    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
        // "Dummy" NOP to allow source level single
        // stepping of tight while() loop
        __asm volatile ("nop");
    }
    return 0 ;
}
