#pragma once

#include "board.h"

#define SPI_BUF_SIZE 262
#define DISKBUF_SIZE 4096

extern SPI_DATA_SETUP_T XfSetup;
extern uint16_t TxBuf[SPI_BUF_SIZE];
extern uint16_t RxBuf[SPI_BUF_SIZE];
extern uint8_t DiskBuf[DISKBUF_SIZE];

void Init_SPI(void);

unsigned int SPI_Read_Status_Reg_1(void);
void SPI_Set_Write_Enable_Latch(void);
uint16_t* SPI_Read_Sector(const uint32_t, const uint32_t);
void SPI_Write_Sector(const uint32_t);
void SPI_Erase(const uint32_t address);
